import React from "react";
import logo from '../img/logo.png';
import Select from 'react-select';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import './header.css';

function Header () {
    let options = [
        {value: 'Киев', label: 'Киев'},
        {value: 'Чернигов', label: 'Чернигов'},
        {value: 'Севастополь', label: 'Севастополь'},
        {value: 'Симферополь', label: 'Симферополь'},
        {value: 'Луганск', label: 'Луганск'},
        {value: 'Донецк', label: 'Донецк'},
        {value: 'Николаев', label: 'Николаев'},
        {value: 'Мариуполь', label: 'Мариуполь'},
        {value: 'Винница', label: 'Винница'},
        {value: 'Макеевка', label: 'Макеевка'},
        {value: 'Херсон', label: 'Херсон'},
        {value: 'Полтава', label: 'Полтава'},
        {value: 'Хмельницкий', label: 'Хмельницкий'},
        {value: 'Черкассы', label: 'Черкассы'},
        {value: 'Черновцы', label: 'Черновцы'},
        {value: 'Житомир', label: 'Житомир'},
        {value: 'Харьков', label: 'Харьков'},
        {value: 'Одесса', label: 'Одесса'},
        {value: 'Днепр', label: 'Днепр'},
        {value: 'Запорожье', label: 'Запорожье'},
        {value: 'Львов', label: 'Львов'},
        {value: 'Кривой Рог', label: 'Кривой Рог'},
        {value: 'Сумы', label: 'Сумы'},
        {value: 'Тернополь', label: 'Тернополь'},
        {value: 'Ровно', label: 'Ровно'},
        {value: 'Горловка', label: 'Горловка'},
        {value: 'Ивано-Франковск', label: 'Ивано-Франковск'},
        {value: 'Каменское', label: 'Каменское'},
        {value: 'Кропивницкий', label: 'Кропивницкий'},
        {value: 'Кременчуг', label: 'Кременчуг'},
        {value: 'Луцк', label: 'Луцк'},
        {value: 'Белая Церковь', label: 'Белая Церковь'},
        {value: 'Керчь', label: 'Керчь'},
        {value: 'Мелитополь', label: 'Мелитополь'},
        {value: 'Краматорск', label: 'Краматорск'},
        {value: 'Ужгород', label: 'Ужгород'},
        {value: 'Бровары', label: 'Бровары'},
        {value: 'Евпатория', label: 'Евпатория'},
        {value: 'Бердянск', label: 'Бердянск'},
        {value: 'Никополь', label: 'Никополь'},
        {value: 'Славянск', label: 'Славянск'},
    ]

    return(<>
        <section className="header">
            <div className="wrapper">
                <div className="header-body">
                    <action className = 'header-logo'>
                        <img src = {logo} alt = 'logo'/>
                        <Select options = {options} placeholder = 'Вкажіть Ваше місто...' className = 'city-input'/>
                    </action>
                    <action>
                        <button type="button" className="header-btn login-btn">Увійти</button>
                        <button type="button" className="header-btn basked-btn">Кошик</button>
                    </action>
                </div>
            </div>
        </section>
        <section className = 'header-bottom'>
            <div className="wrapper">
                <h2 className="header-title">Кухня от дяді Васі</h2>
            </div>
        </section>
    </>)
}

export default Header